/*
 * This work is licensed under the Creative Commons Attribution 3.0 Unported
 * License. To view a copy of this license, visit
 * 
 * http://creativecommons.org/licenses/by/3.0/ or send a letter to
 * 
 * Creative Commons, 444 Castro Street, Suite 900
 * Mountain View, California, 94041, USA.
 */

package org.virtage.slf4jbridges;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.ILogListener;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>Offers easy way to log all Eclipse log events from all installed plug-in
 * to SLF4J logging facade.</p>
 * 
 * <p>Static method {@link #register(BundleContext)} will search for all
 * installed bundles in OSGi environment and
 * create and attach <tt>org.eclipse.core.runtime.ILogListener</tt> writing
 * every log event to SLF4J Logger.</p>
 * 
 * <p>This factory method is should be invoked as soon as possible at the very
 * beginning of the application life-cycle, e.g. in <tt>Activator.start()</tt>.
 * </p> 
 * 
 * <p>Conversion IStatus severity levels to SLFJ4 equivalents:
 * <table border="1">
 * <tr><th>IStatus</th><th>SLF4J</th></tr>
 * <tr><td>ERROR</td><td>ERROR</th></tr>
 * <tr><td>WARNING</td><td>WARN</th></tr>
 * <tr><td>INFO</td><td>INFO</th></tr>
 * <tr><td>OK or CANCEL</td><td>DEBUG</th></tr>
 * <tr><td>(no equivalent)</td><td>TRACE</th></tr>
 * </table></p>
 * 
 * <p>You have to manually free up managed listeners at the application end of
 * life with {@link #unregister()} method. Usual place to call this method is in
 * <tt>Activator.stop()</tt>.
 * 
 * <p>Complete example of typical usage idiom can be akin to this:
 * <pre>
 * public class Activator extends AbstractUIPlugin {
 *     &#64;Override
 *     public void start(BundleContext context) throws Exception {
 *         ...		
 *         SLF4JforEclipse.register(context);
 *    }
 *    
 *     &#64;Override
 *     public void stop(BundleContext context) throws Exception {
 *         // Best practice is to put stopping code in try-finally 
 *         try {
 *             SLF4JforEclipse.unregister();
 *         } finally {
 *             super.stop(context);
 *         }
 *     }
 * }
 * </pre> 
 * 
 * @author Libor Jelinek, ljelinek@virtage.com
 * @see https://bitbucket.org/virtage/slf4j-bridges-for-eclipse-and-osgi *
 */
public final class SLF4JforEclipseBridge {	
	
	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------
	private static SLF4JforEclipseBridge itself;
	
	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------

	/** List of ILogListener instances for installed bundles. */
	private List<LogListener> listeners;
	
	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------
	
	/** Just initialize {@link #listeners} list. */
	private SLF4JforEclipseBridge() {
		this.listeners = new ArrayList<>();
	}
	
	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------	
	
	/**
	 * Starts listening for Eclipse and OSGi log events and repeat them
	 * to SLF4J.
	 * 
	 * Method creates internal list of log listeners writing every Eclipse log event to
	 * SLF4J. At the application end of life, call {@link #unregister()} to release
	 * listeners.
	 * 
	 * @param bundleContext
	 * @return
	 * @throws IllegalArgumentException if invoked without Eclipse Platform running 
	 */
	public static void register(BundleContext bundleContext) {
		if (!Platform.isRunning()) {
			throw new IllegalStateException("Eclipse Platform is not running. " +
					"If you have run this method from pure OSGi environemnt, " +
					"use SLF4JforOSGi instead.");
		}
		
		SLF4JforEclipseBridge instance = new SLF4JforEclipseBridge();
		
		// Iterate throughout all bundles
		for (Bundle bundle : bundleContext.getBundles()) {
			// Attach log listener for its ILog			
			ILog pluginILog = Platform.getLog(bundle);				
			
			instance.listeners.add(new LogListener(pluginILog, bundle));
			
			// Log it
			Logger logger = LoggerFactory.getLogger(SLF4JforEclipseBridge.class);
			logger.debug("Attached SLF4JforEclipse to bundle '{}'.",
					bundle.getSymbolicName());
		}
		
		SLF4JforEclipseBridge.itself = instance;
		
		// Eclipse environment is also OSGi framework, so
		// register also OSGi bridge:
		SLF4JforOSGiBridge.register(bundleContext);
	}
	
	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	/**
	 * Disposes all listeners created with {@link #register(BundleContext)}.
	 * Method is called on the application end of life (e.g. Activator.stop()).
	 * 
	 **/
	public static void unregister() {
		for (LogListener listener : itself.listeners) {
			listener.dispose();
			
			// Log it
			Logger rootLogger = LoggerFactory.getLogger(SLF4JforEclipseBridge.class);			
			rootLogger.debug("Detaching SLF4JforEclipse from bundle '{}'.",
					// Logger name is set to be bundle name in constructor
					listener.slf4jLogger.getName());
		}
		
		// Unregister also OSGi bridge
		SLF4JforOSGiBridge.unregister();
	}
	
	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------
	
	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
		
	//--------------------------------------------------------------------------
	// ILogListener implementation doing actual job
	//--------------------------------------------------------------------------
	
	/**
	 * Class skeleton adopted from PluginLogListener class at 
	 * http://www.ibm.com/developerworks/library/os-eclog/
	 */
	private static class LogListener implements ILogListener {
		
		private ILog pluginILog;
		private Logger slf4jLogger;

		public LogListener(ILog pluginILog, Bundle bundle) {
			this.pluginILog = pluginILog;
			this.slf4jLogger = LoggerFactory.getLogger(bundle.getSymbolicName());
			
			pluginILog.addLogListener(this);
		}		

		@Override
		public void logging(IStatus status, String plugin) {
			if (null == this.slf4jLogger || null == status) {
				return;
			}
			
			plugin = formatText(plugin);
			String statusPlugin = formatText(status.getPlugin());
			String statusMessage = formatText(status.getMessage());
			StringBuffer message = new StringBuffer();
			
			if (plugin != null) {
				message.append(plugin);
				message.append(" - ");
			}
			
			if (statusPlugin != null
					&& (plugin == null || !statusPlugin.equals(plugin))) {
				message.append(statusPlugin);
				message.append(" - ");
			}
			
			message.append(status.getCode());
			if (statusMessage != null) {
				message.append(" - ");
				message.append(statusMessage);
			}
			
			
			// Conversion IStatus severity to SLF4J
			int severity = status.getSeverity();
			
			if (severity == Status.ERROR) {
				this.slf4jLogger.error(message.toString(), status.getException());
				
			} else if (severity == Status.WARNING) {
				this.slf4jLogger.warn(message.toString(), status.getException());
				
			} else if (severity == Status.INFO) {
				this.slf4jLogger.info(message.toString(), status.getException());
				
			} else {
				this.slf4jLogger.debug(message.toString(), status.getException());
				
			}			
		}
		
		public void dispose() {
			if (this.pluginILog != null) {				
				this.pluginILog.removeLogListener(this);
				this.pluginILog = null;
			}
		}
		
		private String formatText(String text) {
			if (text != null) {
				text = text.trim();
				
				if (text.length() == 0) {
					return null;
				}
			}
			
			return text;
		}
		
	}
}