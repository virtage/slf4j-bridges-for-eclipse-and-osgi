/*
 * This work is licensed under the Creative Commons Attribution 3.0 Unported
 * License. To view a copy of this license, visit
 * 
 * http://creativecommons.org/licenses/by/3.0/ or send a letter to
 * 
 * Creative Commons, 444 Castro Street, Suite 900
 * Mountain View, California, 94041, USA.
 */
package org.virtage.slf4jbridges;

import org.eclipse.equinox.log.ExtendedLogEntry;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogEntry;
import org.osgi.service.log.LogListener;
import org.osgi.service.log.LogReaderService;
import org.osgi.service.log.LogService;
import org.osgi.util.tracker.ServiceTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * <p>Offers easy way to log all OSGi log events from all installed plug-in
 * to SLF4J logging facade.</p>
 * 
 * <p>Static method {@link #register(BundleContext)} will search for all
 * installed bundles in OSGi environment and
 * create and attach <tt>org.eclipse.core.runtime.ILogListener</tt> writing
 * every log event to SLF4J Logger.</p>
 * 
 * <p>This factory method is should be invoked as soon as possible at the very
 * beginning of the application life-cycle, e.g. in <tt>Activator.start()</tt>.
 * </p> 
 * 
 * <p>Conversion IStatus severity levels to SLFJ4 equivalents:
 * <table border="1">
 * <tr><th>LogEntry.getLevel()</th><th>SLF4J</th></tr>
 * <tr><td>ERROR</td><td>ERROR</th></tr>
 * <tr><td>WARNING</td><td>WARN</th></tr>
 * <tr><td>INFO</td><td>INFO</th></tr>
 * <tr><td>DEBUG</td><td>DEBUG</th></tr>
 * <tr><td>(no equivalent)</td><td>TRACE</th></tr>
 * </table></p>
 * 
 * <p>You should manually free up managed listeners at the application end of
 * life with {@link #unregister()} method. Usual place to call this method is in
 * <tt>Activator.stop()</tt>.
 * 
 * <p>Complete example of typical usage idiom can be akin to this:
 * <pre>
 * public class Activator implements BundleActivator {
 *     &#64;Override
 *     public void start(BundleContext context) throws Exception {
 *         ...		
 *         SLF4JforOSGi.register(context);
 *    }
 *    
 *     &#64;Override
 *     public void stop(BundleContext context) throws Exception {
 *         // Best practice is to put stopping code in try-finally 
 *         try {
 *             SLF4JforOSGi.unregister();
 *         } finally {
 *             super.stop(context);
 *         }
 *     }
 * }
 * </pre> 
 * 
 * @author Libor Jelinek, ljelinek@virtage.com
 * @see https://bitbucket.org/virtage/slf4j-bridges-for-eclipse-and-osgi Homepage
 */
public final class SLF4JforOSGiBridge implements LogListener {

	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------
	
	private static SLF4JforOSGiBridge itself;
	private static LogReaderService logReaderService;
  
    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------
 
	private SLF4JforOSGiBridge() {}
	
    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------
 
	public static void register(BundleContext bundleContext) {		
		// Find out service from context
		ServiceTracker<LogReaderService, LogReaderService> logReaderServiceTracker = 
				new ServiceTracker<>(bundleContext, LogReaderService.class, null);
		logReaderServiceTracker.open();
        LogReaderService logReaderService = (LogReaderService) logReaderServiceTracker.getService();
        
        // On rare case of unavailability
        if (logReaderService == null) {
        	throw new RuntimeException("OSGi LogReaderService is not available for this bundle context.");
        }
        
        // Otherwise store instances to fields and register itself as a itself
        SLF4JforOSGiBridge.logReaderService = logReaderService; 
        SLF4JforOSGiBridge.itself = new SLF4JforOSGiBridge();
        
        logReaderService.addLogListener(itself);        
	}
	
	public static void unregister() {
		if (itself == null) {
			throw new IllegalStateException("unregister() cannot be called prior register(BundleContext)!");
		}
		
		logReaderService.removeLogListener(itself);
	}
	
    //--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------	
	 
    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------
 
	@Override
	public void logged(LogEntry entry) {	
		// Info from basic LogEntry
		int 	  level 	= entry.getLevel();
		Bundle 	  bundle 	= entry.getBundle();
		Throwable throwable = entry.getException();
		String    message   = entry.getMessage();
		ServiceReference<?> serviceReference = entry.getServiceReference();
		
		if (bundle != null) MDC.put("bundle", bundle.toString());
		if (serviceReference != null) MDC.put("serviceReference", serviceReference.toString());
		
		// Logging to root logger by default
		Logger logger = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME); 
		
		// But for Equinox 3.6+ ExtendedLogEntry, a logger per bundle
		if (entry instanceof ExtendedLogEntry) {			
			logger = LoggerFactory.getLogger(bundle.getSymbolicName());
			
			// Extract additional info
			ExtendedLogEntry exEntry = (ExtendedLogEntry) entry;
			
			Object context    = exEntry.getContext();
			long   threadId   = exEntry.getThreadId();
			String threadName = exEntry.getThreadName();
						
			// Put it in MDC
			if (context != null) MDC.put("context", context.toString());			
			if (threadName!= null) MDC.put("threadName", threadName);	
			MDC.put("threadId", String.valueOf(threadId));
		}
		
		// Construct message
		if (message == null) message = "(empty message)";
		
		// Decide about severity
		if (level == LogService.LOG_DEBUG) {
			if (throwable != null)
				logger.debug(message, throwable);
			else
				logger.debug(message);
			
		} else if (level == LogService.LOG_INFO) {
			if (throwable != null)
				logger.info(message, throwable);
			else 
				logger.info(message);
			
		} else if (level == LogService.LOG_WARNING) {
			if (throwable != null)
				logger.warn(message, throwable);
			else
				logger.warn(message);
			
		} else if (level == LogService.LOG_ERROR) {
			if (throwable != null)
				logger.error(message, throwable);
			else
				logger.error(message);
		} else {
			throw new RuntimeException("Illegal severity level " + level + "!");
		}		
	}
	
    //--------------------------------------------------------------------------
    // Consider toString(), equals(), hashCode()
    //--------------------------------------------------------------------------
 
	/** Method is, in particular, used in as name of logger in log output. */
	@Override
	public String toString() {
		return "SLF4JforOSGiBridge";
	}
	
    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------
}
